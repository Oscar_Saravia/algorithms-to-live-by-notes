Danny Hillis: MIT is an undergraduate, his roommate is bad at going through socks 

Took over 100 looks in the bin in order to sort all the socks 

In the 1800’s there was a 30% population growth  

Hermon Hollerith was inspired by railroad tickets. He created a system of punched cards and a machine to sort them. The machine was named the Hollerith Machine. The government adopted Hermon’s system

Hollerith's firm became the Computing Tabulating Recording Company in 1911, they were later renamed International Business Machines (IBM)  

First code ever written for a stored program computer was made for sorting  

By the 1960's, more than ¼ of computing resources were being spent on sorting  

With sorting, the more there is the harder it gets  

In computer science, there is a way to measure algorithmic worst case scenarios called Big O Nation. It shows the relationship between the size of the problem and the problem's runtime  

Another sorting technique is called bubble sort, it’s quadratic(and slow). Bubble sort is basically like if you sorted a bookshelf by switching two at a time and restarting every time. It would mean your taking 25 times as long to sort 5 stacks.

Another sorting technique is called the insertion sort. You take all the books off the bookshelves and insert them in the right spot one by one. It's not much faster than bubble sort

In 1936 IBM created a line of machines called collators. They would sort 2 piles into one - in linear time The program was made to demonstrate the power of the stored program computer 

In 1945 John Von Neumann was the one to take this idea of sorting and apply it. You just put the smaller value on top (out of two packs merging into one.). This was called MergeSort

The Preston Library is one of the best book sorters in the world, they use the bucket sort method

Charles Lutwidge Dodgson (Lewis Carroll) a mathematics lecturer at oxford pointed out that silver is a lie in matches where a single elimination means tournament elimination

Round Robin Tournaments (where everyone fights everyone) is quadratic
The ladder method (where they fight the people better than them and if they win they switch places) is also quadratic

Math shows that there is very little chance that the actual best team will win. Scientist calls this phenomenon noise. The best algorithm in face of noise is called the comparison counting sort. Each item is compared with all the others, generating a tally of how many items it is bigger than.    

All these examples have been ordering so that the smallest is put on top. That is called displacement. It is using your knowledge of the hierarchy to determine when a confrontation isn't worth it (neumann). If the hierarchy doesn't exist there's more problems

Unlike chickens - determining hierarchy doesn't have to be bloody. The bigger fish is the dominant fish. Even though its a rat race at least it's a race and not a fight

Cardinal: counting numbers. Ex: (1, 2, 3....)
Ordinal: Orderly numbers. Ex: (first, second, third....)



